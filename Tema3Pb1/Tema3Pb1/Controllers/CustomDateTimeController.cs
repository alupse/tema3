﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tema3Pb1.Models;

namespace Tema3Pb1.Controllers
{
    public class CustomDateTimeController : Controller
    {

        CustomDateTime _customDateTime = new CustomDateTime();

        public CustomDateTimeController()
        {
            _customDateTime.Moment = DateTime.Now;

        }

        public IActionResult Index()
        {
            var customDataTime = _customDateTime;
            return View(customDataTime);
        }
    }
}