﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tema3Ex2.Models;

namespace Tema3Ex2.Controllers
{
    public class ProductController : Controller
    {

        readonly ProductRepository _repo;

        public ProductController(ProductRepository repo)
        {
            _repo = repo;
        }

        public IActionResult GetAll()
        {
            var products = _repo.Products;
            return View(products);
        }

        public IActionResult GetById(int id)
        {
            for(int i=0;i<3;i++)
            {
                if (id == _repo.Products[i].Id)
                {
                    return View(_repo.Products[i]);
                }
            }

            return View();
            
        }
    }
}