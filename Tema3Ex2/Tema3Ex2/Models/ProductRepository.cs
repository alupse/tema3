﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tema3Ex2.Models
{
    public class ProductRepository
    {
        readonly List<Product> _products = new List<Product>
        {
            new Product
            {
                Id=1,
                Name="Jacket",
                Description="A jacket is a mid-stomach–length garment for the upper body."
            },
             new Product
            {
                Id=2,
                Name="Jeans",
                Description="Jeans are a type of pants or trousers, typically made from denim or dungaree cloth."
            },
              new Product
            {
                Id=3,
                Name="Shoes",
                Description="A shoe is an item of footwear intended to protect and comfort the human foot, while the wearer is doing various activities. "
            }


        };
        public List<Product> Products => _products;
    }
    
}
